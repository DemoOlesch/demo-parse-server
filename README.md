Description
==

Deploy parse server to staging and production environments with a REST API and dashboard website for viewing your data in 30 minutes or less.

Deploy it to Heroku with Bitbucket Pipelines
==
1. Create two apps on Heroku; one for staging and one for production.
2. Add an mLab MongoDB resource to each Heroku app.
1. Fill in these config variables for both heroku apps:
    * APP_ID: an identifier for your app that's passed as a header in API requests.
    * APP_NAME: the app name to display in the dashboard.
    * SERVER_URL: the URL of your API on Heroku starting with https and ending with /parse.
    * DASHBOARD_USERNAME: the username for your dashboard.
    * DASHBOARD_PASSWORD: the password for your dashboard.
    * MASTER_KEY: the key used to override permissions set by your app.
1. Create a new Bitbucket repository and fill in these environment variables in your repository settings
	* HEROKU_API_KEY: the API key from https://dashboard.heroku.com/account
	* HEROKU_STAGING_APP_NAME: the staging app name from https://dashboard.heroku.com/apps
	* HEROKU_PRODUCTION_APP_NAME the production app name from https://dashboard.heroku.com/apps
1. Push this repository to the Bitbucket repository
1. Open the server root URL without /parse at the end. You should see "Everything is OK!"

This code base running on Heroku: https://standard-parse-server.herokuapp.com.

Run it locally
==
Make sure you have all dependencies installed. Then run 

```
#!bash

npm start
```

Contribution
==
Bug reports and pull requests are welcome.

Contributors
==
David Olesch @ Jackrabbit Mobile

Alternatives
==
Parse has their own example project which may be more up to date at some point but it requires a lot of changes to work how I want it https://github.com/ParsePlatform/parse-server-example